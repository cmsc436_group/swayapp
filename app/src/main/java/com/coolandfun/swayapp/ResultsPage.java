package com.coolandfun.swayapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import edu.umd.cmsc436.sheets.Sheets;

public class ResultsPage extends AppCompatActivity implements Sheets.Host{

    private ListView mainListView ;
    private ArrayAdapter<String> listAdapter ;
    int scores[] = new int[100];
    int index, count;
    Sheets sheet;
    String user = "t03p02";
    public static final int LIB_ACCOUNT_NAME_REQUEST_CODE = 1001;
    public static final int LIB_AUTHORIZATION_REQUEST_CODE = 1002;
    public static final int LIB_PERMISSION_REQUEST_CODE = 1003;
    public static final int LIB_PLAY_SERVICES_REQUEST_CODE = 1004;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_page);
        mainListView = (ListView) findViewById(R.id.list);
        scores = getIntent().getExtras().getIntArray("scores");
        index = getIntent().getExtras().getInt("index");
        List<String> scoresList = new ArrayList<String>(scores.length);
        for (int i = 0; i < index; i++) {
            scoresList.add("Trial "+String.valueOf(i)+": "+String.valueOf(scores[i]));
        }
        listAdapter = new ArrayAdapter<String>(this, R.layout.list_row, scoresList);
        mainListView.setAdapter( listAdapter );
        Button upload = (Button) findViewById(R.id.upload_btn);
        upload.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {saveNextToSheets();}
        });
        Button back = (Button) findViewById(R.id.back_btn);
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {returnToMenu();}
        });
        count = 0;
        sheet = new Sheets(this, getString(R.string.app_name), getString(R.string.CMSC436Sheet_spreadsheet_id_test_sheet));
    }

    private void saveNextToSheets(){
        Log.i(String.valueOf(scores[count]), "score");
        sheet.writeData(Sheets.TestType.RH_LEVEL, user, (float)scores[count]);
        count++;
    }
    private void returnToMenu(){
        Intent intent = new Intent(ResultsPage.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        sheet.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sheet.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public int getRequestCode(Sheets.Action action) {
        switch (action) {
            case REQUEST_ACCOUNT_NAME:
                return LIB_ACCOUNT_NAME_REQUEST_CODE;
            case REQUEST_AUTHORIZATION:
                return LIB_AUTHORIZATION_REQUEST_CODE;
            case REQUEST_PERMISSIONS:
                return LIB_PERMISSION_REQUEST_CODE;
            case REQUEST_PLAY_SERVICES:
                return LIB_PLAY_SERVICES_REQUEST_CODE;
            default:
                return -1;
        }
    }

    @Override
    public void notifyFinished(Exception e) {
        if (e != null) {
            throw new RuntimeException(e);
        }
        Log.i(getClass().getSimpleName(), "Done");
        if(count != index){
            saveNextToSheets();
        }
    }
}
