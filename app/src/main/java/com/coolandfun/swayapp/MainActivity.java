package com.coolandfun.swayapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button cont;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cont = (Button)findViewById(R.id.cont_btn);

        cont.setOnClickListener(this);
    }

    private void continueButtonClick(){
        Intent intent = new Intent(MainActivity.this, SwayActivity.class);
        startActivity(intent);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                settingsButtonClick();
                return true;
            case R.id.results:
                resultButtonClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void resultButtonClick() {
        Intent intent = new Intent(this, ResultsPage.class);
        startActivity(intent);
    }

    private void settingsButtonClick() {
        Intent intent = new Intent(this, SettingsPage.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.cont_btn:
                continueButtonClick();
                break;
        }
    }
}
