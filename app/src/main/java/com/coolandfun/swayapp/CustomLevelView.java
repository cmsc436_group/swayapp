package com.coolandfun.swayapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.SensorEvent;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

public class CustomLevelView extends View {
    private int x, z, xoffset, zoffset,rBubble, rTarget, originX, originZ,  width, height, lastDiffX, lastDiffZ;
    private final Paint bubbleColor = new Paint();
    private final Paint targetColor = new Paint();
    private final Paint textColor = new Paint();
    private Paint tempUserSpiralPaint = new Paint();;
    private Trace userTrace;
    private double score = 0;
    private boolean scoring, init  = false;
    private final double rewardGood = 0.1;
    private final double rewardEh = 0;
    private final double rewardBad = -0.1;


    public CustomLevelView(Context context,  AttributeSet attrs) {
        super(context, attrs);
        userTrace = new Trace(getWidth()/2, getHeight()/2);
        bubbleColor.setColor(0xFF00BFFF);
        textColor.setColor(0xFF000000);
        textColor.setTextSize(64);
        lastDiffX = lastDiffZ = 0;

        tempUserSpiralPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tempUserSpiralPaint.setColor(getResources().getColor(R.color.colorPrimary, getContext().getTheme()));
        } else {
            //noinspection deprecation
            tempUserSpiralPaint.setColor(getResources().getColor(R.color.colorPrimary));
        }
        tempUserSpiralPaint.setStyle(Paint.Style.STROKE);
        tempUserSpiralPaint.setStrokeWidth(5);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.width= w;
        this.height = h;
        this.x = this.originX = width/2;
        this.z = this.originZ = height/2;
        this.rBubble = h/32;
        this.rTarget = h/12;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        targetColor.setColor(0xFF000000);
        targetColor.setTextSize(48f);
        canvas.drawRect((this.originX - this.originX/2), this.originZ - 10, (this.originX + this.originX/2), this.originZ + 10, targetColor);
        canvas.drawRect(this.originX- 10, (this.originZ - this.originZ/2), this.originX + 10, (this.originZ + this.originZ/2), targetColor);
        canvas.drawText("X", (this.originX + this.originX/2 + 20), this.originZ + 10, targetColor);
        canvas.drawText("Z", this.originX - 10, (this.originZ - this.originZ/2 - 20), targetColor);
        canvas.drawCircle(this.x, this.z, this.rBubble, this.bubbleColor);
        String scoreStr = String.valueOf((int)this.score);
        canvas.drawText(scoreStr,0, scoreStr.length(), width/2 , height, this.textColor);
        for (int i = 0; i < userTrace.getNTraces(); i += 1) {
            tempUserSpiralPaint.setColor(0xFF0000FF);
            canvas.drawPath(userTrace.getPaths().get(i), tempUserSpiralPaint);
        }
        invalidate();
    }

    private void setScore(){
        if ((Math.abs(this.originX - this.x) + rBubble < rTarget)
                && (Math.abs(this.originZ - this.z) + rBubble < rTarget)) {
            score += rewardGood*3;
        } else {
            if ((Math.abs(this.originX - this.x) + rBubble < rTarget*2)
                    && (Math.abs(this.originZ - this.z) + rBubble < rTarget*2)) {
                score += rewardGood*2;
            }else {
                if ((Math.abs(this.originX - this.x) + rBubble < rTarget*3)
                        && (Math.abs(this.originZ - this.z) + rBubble < rTarget*3)) {
                    score += rewardGood*1;
                }else {
                    if (lastDiffX > (Math.abs(this.originX - this.x))
                            || lastDiffZ > (Math.abs(this.originZ - this.z))) {
                        score += rewardEh;
                    } else {
                        score += rewardBad;
                    }
                }
            }
        }
    }

    public int getScore(){
        return (int)score;
    }

    public void onSensorEvent(SensorEvent event) {
        if(scoring) {
            if(!init){
                xoffset = (int) event.values[0];
                zoffset = (int) event.values[2];
                init = true;
            }else{
                lastDiffX = (Math.abs(this.originX - this.x));
                lastDiffZ = (Math.abs(this.originZ - this.z));
                x -= ((int) event.values[0] - xoffset);
                z += ((int) event.values[2] - zoffset);
                if (x <= 0 + rBubble) {
                    x = 0 + rBubble;
                }
                if (x >= width - rBubble) {
                    x = width - rBubble;
                }
                if (z <= 0 + rBubble) {
                    z = 0 + rBubble;
                }
                if (z >= height - rBubble) {
                    z = height - rBubble;
                }
                if (scoring) {
                    setScore();
                    userTrace.add((float) x, (float) z, System.nanoTime());
                }
            }
        }
    }
    public Trace getUserTrace() {
        return userTrace;
    }
    public void clearTrace(){userTrace.clear();}
    public void startGame(){
        scoring = true;
    }
    public void endGame(){
        scoring = false;
        this.x = this.originX = width/2;
        this.z = this.originZ = height/2;
        score = 0;
    }
    public void onSave (Canvas canvas) {
        super.draw(canvas);
        draw(canvas);
    }
}
