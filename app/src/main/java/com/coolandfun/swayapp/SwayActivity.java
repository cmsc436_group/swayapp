package com.coolandfun.swayapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SwayActivity extends AppCompatActivity implements SensorEventListener {

    private CustomLevelView bubble;
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private int time_left = 25;
    private int time_left_temp;
    private int trialAmount = 2;
    private int tempTrialAmount;
    private TextView timer_text;
    private static Context context;
    private int index = 0;
    private int[] scores = new int[100];
    Vibrator v;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sway);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        context = getApplicationContext();
        bubble = (CustomLevelView) findViewById(R.id.bubble_view);
        senSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        timer_text = (TextView) findViewById(R.id.timer);
        time_left_temp = time_left;
        tempTrialAmount = trialAmount;
        v = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);
        runGame();
    }
    @Override
    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            bubble.onSensorEvent(event);
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    private void runGame(){
        if (bubble.getScore() == 0) {
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    if (time_left_temp == 0) {
                        scores[index++] = bubble.getScore();
                        timer_text.setText("Total Score: "+String.valueOf(scores[index - 1]));
                        bubble.endGame();
                        bubble.clearTrace();
                        time_left_temp = time_left;
                        if(tempTrialAmount > 1){
                            tempTrialAmount -= 1;
                            runGame();
                    }else{
                            tempTrialAmount = trialAmount;
                            GameFinished();
                        }
                    } else if (time_left_temp > time_left - 5){
                        timer_text.setText("Beginning in "+String.valueOf(time_left_temp-(time_left-5))+" seconds");
                        time_left_temp -= 1;
                        final Handler h = new Handler();
                        h.postDelayed(this, 1000);
                    } else {
                        if(time_left_temp == time_left - 5){
                            bubble.startGame();
                            v.vibrate(500);
                        }
                        timer_text.setText(String.valueOf(time_left_temp) + " seconds");
                        time_left_temp -= 1;
                        final Handler h = new Handler();
                        h.postDelayed(this, 1000);
                    }
                }
            };

            task.run();
        }
    }

    private String getDateTime () {
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
        return dateFormat.format(new Date(System.currentTimeMillis()));
    }

    private void saveTracerView() {

        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            // External media not mounted, so nothing to write
            return;
        }

        Bitmap bm = Bitmap.createBitmap(bubble.getWidth(), bubble.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bm);
        bubble.onSave(c);

        String dateTime = getDateTime();
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "SpiralTrial_" + dateTime);
        if (!dir.mkdirs()) {
            // error when creating directory
            return;
        }
        File imageFile = new File(dir, getString(R.string.bubble_trial_name));
        File userCSV = new File(dir, getString(R.string.user_bubble_data_name));

        try {
            FileOutputStream out = new FileOutputStream(imageFile);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

            bubble.getUserTrace().writeToFile(userCSV);

            Toast.makeText(this, "Saved " + dateTime, Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Log.e(this.getClass().toString(), e.getMessage());
        }
    }
    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("data.josh", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    private void GameFinished(){
        Intent intent = new Intent(SwayActivity.this, ResultsPage.class);
        intent.putExtra("scores", this.scores);
        intent.putExtra("index", this.index);
        startActivity(intent);
    }
}
